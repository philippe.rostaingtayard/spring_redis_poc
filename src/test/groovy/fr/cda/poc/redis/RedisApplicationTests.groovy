package fr.cda.poc.redis

import fr.cda.poc.redis.domain.Article
import fr.cda.poc.redis.domain.Cart
import fr.cda.poc.redis.domain.CartResource
import fr.cda.poc.redis.domain.Variant
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.redis.core.RedisKeyValueTemplate
import org.springframework.data.redis.core.RedisTemplate
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
class RedisApplicationTests extends Specification {

    @Autowired
    RedisKeyValueTemplate redisKeyValueTemplate

    @Autowired
    RedisTemplate<?, ?> redisTemplate

    @Shared
    Cart cart

    @Shared
    CartResource cartResource

    def setup() {
        cart = new Cart(
                id: UUID.randomUUID(),
                articles: [
                        new Article(
                                id: 'a1',
                                type: 'ARTICLE',
                                variants: [
                                        new Variant(id: 'v1', qty: 10),
                                        new Variant(id: 'v2', qty: 11),
                                ]
                        ),
                        new Article(
                                id: 'a2',
                                type: 'ARTICLE',
                                variants: [
                                        new Variant(id: 'v1', qty: 20),
                                        new Variant(id: 'v2', qty: 21),
                                ]
                        ),
                ]
        )

        cartResource = new CartResource(cart)
    }

    @Test
    void contextLoads() {
        when:
            redisTemplate.opsForHash().putAll(cartResource.id, cartResource.flatCart)
            def cartResource = redisTemplate.opsForHash()
        then:
            cartResource

    }

}
