package fr.cda.poc.redis.domain

import spock.lang.Specification

class CartResourceTest extends Specification {

    def "create a Cart from a CartResource"() {
        given: 'a resource'
        CartResource cartResource = new CartResource(id: UUID.randomUUID(), flatCart: [
                'id': UUID.randomUUID() as String,
                'aid-a1': 'a1',
                'atype-a1': 'type-1',
                'aid-a2': 'a2',
                'atype-a2': 'type-2',
                'vid-a1-v1': 'v1',
                'vqty-a1-v1': '10',
                'vid-a1-v2': 'v2',
                'vqty-a1-v2': '20',
                'vid-a2-v2': 'v2',
                'vqty-a2-v2': '11'
        ])

    when: 'create map'
        def cart = cartResource.toCart()

    then:
        assert cart
        with(cart) {
            id == UUID.fromString(cartResource.id)
            articles.size() == 2
            def article = articles.find { it.id == 'a1'}
            with(article) {
                id == 'a1'
                type == 'type-1'
                variants.size() == 2
            }
        }
    }

    def 'create a CartResource from a Cart'(){
        given: 'a cart'
            def id = UUID.randomUUID()
            def cart = new Cart(
                id: id,
                articles: [
                    new Article(
                        id: 'a1',
                        type: 'ARTICLE',
                        variants: [
                            new Variant(id: 'v1', qty: 10),
                            new Variant(id: 'v2', qty: 11),
                        ]
                    ),
                    new Article(
                        id: 'a2',
                        type: 'ARTICLE',
                        variants: [
                            new Variant(id: 'v1', qty: 20),
                            new Variant(id: 'v2', qty: 21),
                        ]
                    ),
                ]
            )

        when: 'create a CartResource'
            def cartResource = new CartResource(cart)

        then:
            cartResource.id == id as String
            cartResource.flatCart.size() == 12
            with(cartResource) {
                flatCart['aid-a1'] == 'a1'
                flatCart['atype-a1'] == 'ARTICLE'
                flatCart['vid-a1-v1'] == 'v1'
                flatCart['vqty-a1-v1'] == 10
            }
    }
}
