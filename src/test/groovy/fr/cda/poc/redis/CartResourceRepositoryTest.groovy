package fr.cda.poc.redis

import fr.cda.poc.redis.domain.CartResource
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class CartResourceRepositoryTest extends Specification {

    @Autowired
    CartResourceRepository cartResourceRepository

    def "Save"() {
        given: 'a cart resource'
            CartResource cartResource = new CartResource(id: UUID.randomUUID(), flatCart: [
                    'id': UUID.randomUUID() as String,
                    'aid-a1': 'a1',
                    'atype-a1': 'type-1',
                    'aid-a2': 'a2',
                    'atype-a2': 'type-2',
                    'vid-a1-v1': 'v1',
                    'vqty-a1-v1': '10',
                    'vid-a1-v2': 'v2',
                    'vqty-a1-v2': '20',
                    'vid-a2-v2': 'v2',
                    'vqty-a2-v2': '11'
            ])
        when: 'save cartResource'
            cartResourceRepository.save(cartResource)
        then:
            true

    }

    def "Get"() {
    }
}
