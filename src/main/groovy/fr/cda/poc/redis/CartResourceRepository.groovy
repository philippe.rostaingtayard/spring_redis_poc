package fr.cda.poc.redis

import fr.cda.poc.redis.domain.Cart
import fr.cda.poc.redis.domain.CartResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.RedisKeyValueTemplate
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Repository

@Repository
class CartResourceRepository {

//    RedisKeyValueTemplate redisKeyValueTemplate
    RedisTemplate redisTemplate
    Cart cart

    CartResourceRepository(
            //RedisKeyValueTemplate redisKeyValueTemplate,
            RedisTemplate redisTemplate
    ) {
//        this.redisKeyValueTemplate = redisKeyValueTemplate
        this.redisTemplate = redisTemplate
    }

    void save(CartResource cartResource) {
        redisTemplate.opsForHash().putAll(cartResource.id, cartResource.flatCart)
    }

    CartResource get (String id) {
        def cartResource = new CartResource(id: id)
        def keys = redisTemplate.opsForHash().keys(id)
        keys.each { key ->
            def value = redisTemplate.opsForHash().get(id, key)
            cartResource.flatCart.put(key, value)
        }
        cartResource
    }
}
