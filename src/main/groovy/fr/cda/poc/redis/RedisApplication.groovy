package fr.cda.poc.redis

import fr.cda.poc.redis.domain.Article
import fr.cda.poc.redis.domain.Cart
import fr.cda.poc.redis.domain.CartResource
import fr.cda.poc.redis.domain.Variant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.PartialUpdate
import org.springframework.data.redis.core.RedisKeyValueTemplate
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.StringRedisTemplate

@SpringBootApplication
class RedisApplication {

    @Autowired
    RedisKeyValueTemplate redisKeyValueTemplate

    @Autowired
    RedisTemplate redisTemplate

    static void main(String[] args) {
        SpringApplication.run(RedisApplication, args)
    }

    @Bean
    CommandLineRunner commandLineRunner(StringRedisTemplate stringRedisTemplate, CartRepository cartRepository, RedisTemplate hashRedisTemplate) {

//        // on teste pour
//        stringRedisTemplate.opsForValue().set('key', 'value')
//        def value = stringRedisTemplate.opsForValue().get('key')
//        assert value == 'value'

//        assert cartRepository

//        def id = UUID.randomUUID()
//        def cartInit = new Cart(id: id)
//        createCart(cartRepository, cartInit)
//        def cart = cartRepository.findById(id).get()
//        assert cart
//        assert cart.id == cartInit.id

//        PartialUpdate<Cart> update = PartialUpdate.newPartialUpdate('cart-1', Cart.class)
//            .set('name', 'nom')
//            .set('tick-1', 'TICKET 1')
//        redisKeyValueTemplate.update(update)
//
//        def cart = cartRepository.findById("cart-1").get()
//        assert cart.name == 'nom'
//
//        def to

//        def article = new Article(id: 'a1', variants: [new Variant(id: 'a1v1', qty: 12)])
//        PartialUpdate<Cart> updateCart = PartialUpdate.newPartialUpdate(id, Cart.class)
//            .set('articles', [article])
//        redisKeyValueTemplate.update(updateCart)
//
//        def updatedCart = cartRepository.findById(id).get()
//        assert updatedCart.articles

        def cart = new Cart(
                id: UUID.randomUUID(),
                articles: [
                        new Article(
                                id: 'a1',
                                type: 'ARTICLE',
                                variants: [
                                        new Variant(id: 'v1', qty: 10),
                                        new Variant(id: 'v2', qty: 11),
                                ]
                        ),
                        new Article(
                                id: 'a2',
                                type: 'ARTICLE',
                                variants: [
                                        new Variant(id: 'v1', qty: 20),
                                        new Variant(id: 'v2', qty: 21),
                                ]
                        ),
                ]
        )

        saveCartResource(new CartResource(cart))

        def newCart = getCart(cart.id)
        assert newCart
        assert newCart.id == cart.id
        assert newCart.articles.size() == 2
        println(newCart)
        return null
    }

    def saveCartResource(CartResource cartResource) {
        redisTemplate.opsForHash().putAll(cartResource.id, cartResource.flatCart)
    }

    Cart getCart (UUID uuid) {
        String id = uuid as String
//        String id = redisTemplate.opsForHash().get(id, 'id')
        def cartResource = new CartResource(id: id)
        def keys = redisTemplate.opsForHash().keys(id)
        keys.each { key ->
            def value = redisTemplate.opsForHash().get(id, key)
            cartResource.flatCart.put(key, value)
        }
        cartResource.toCart()
    }

}
