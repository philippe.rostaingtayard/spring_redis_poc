package fr.cda.poc.redis

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.RedisKeyValueTemplate
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories

/**
 * configuration de Spring
 */
@Configuration
@EnableRedisRepositories
class AppConfig {

    /*
     * configuration de la connexion
     */
    @Bean
    LettuceConnectionFactory redisConnectionFactory() {
        new LettuceConnectionFactory(new RedisStandaloneConfiguration("localhost"))
    }

    /*
     * configuration d'un template de string
     */
    @Bean
    StringRedisTemplate stringRedisTemplate() {
        new StringRedisTemplate(redisConnectionFactory())
    }

    /*
     * configuration
     */
    @Primary
    @Bean
    RedisTemplate<?, ?> redisTemplate() {
        RedisTemplate<?, ?> redisTemplate = new RedisTemplate<byte[], byte[]>()
        redisTemplate.connectionFactory = redisConnectionFactory()
        redisTemplate
    }

    RedisKeyValueTemplate redisKeyValueTemplate
}
