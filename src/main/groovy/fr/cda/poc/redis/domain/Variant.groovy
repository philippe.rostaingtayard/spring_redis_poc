package fr.cda.poc.redis.domain

import groovy.transform.ToString

@ToString
class Variant {
    String id
    Integer qty
}

