package fr.cda.poc.redis.domain

import groovy.transform.ToString

@ToString
class Article {
    String id
    String type
    List<Variant> variants = []
}
