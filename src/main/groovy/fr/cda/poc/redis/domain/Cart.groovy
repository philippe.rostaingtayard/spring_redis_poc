package fr.cda.poc.redis.domain

import groovy.transform.ToString
import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

import java.time.LocalDateTime

@ToString
@RedisHash('Cart')
class Cart {
    @Id UUID id
    LocalDateTime dateCreated = LocalDateTime.now()
    LocalDateTime lastUpdated = LocalDateTime.now()
    List<Article> articles = []
}
