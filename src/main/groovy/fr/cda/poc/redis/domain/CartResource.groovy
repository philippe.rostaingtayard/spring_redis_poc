package fr.cda.poc.redis.domain

import org.springframework.data.redis.core.RedisHash

/**
 * représentation pour enregistrement
 * dans une clef redis, dont la clef sera celle du Cart on gerera un
 */
@RedisHash('CartResource')
class CartResource {

    String id
    Map<String, String> flatCart = [:]

    /**
     * pour utiliser le constructeur groovy
     */
    CartResource() {}
    /**
     * construit la représentation en fonction du Cart
     *
     * @param cart
     */
    CartResource(Cart cart) {
        this.id = cart.id as String
//        this.flatCart['id'] = cart.id as String
        cart.articles.each { article ->
            this.flatCart["aid-$article.id"] = article.id
            this.flatCart["atype-$article.id"] = article.type
            article.variants.each { variant ->
                this.flatCart["vid-$article.id-$variant.id"] = variant.id
                this.flatCart["vqty-$article.id-$variant.id"] = variant.qty
            }
        }
    }

    Cart toCart() {
        def articles = []
        flatCart.findAll { it.key.startsWith('aid')}.each { flatArticle ->
            def article = new Article(id: flatArticle.value, type: flatCart["atype-$flatArticle.value"], variants: [])
            flatCart.findAll { it.key.startsWith("vid-$flatArticle.value")}.each { flatVariant ->
                article.variants << new Variant(id: flatVariant.value, qty: flatCart["vqty-$flatArticle.value-$flatVariant.value"] as Integer)
            }
            articles << article
        }
        new Cart(id: UUID.fromString(id), articles: articles)
    }
}
