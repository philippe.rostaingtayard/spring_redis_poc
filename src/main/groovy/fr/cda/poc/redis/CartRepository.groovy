package fr.cda.poc.redis

import fr.cda.poc.redis.domain.Cart
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface CartRepository extends CrudRepository<Cart, String>{

}